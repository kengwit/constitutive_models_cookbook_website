# Constitutive Models Cookbook  
### By Keng-Wit Lim
=================

This is a short elementary text (about 70 pages) on the derivations of constitutive models (metal, soil, concrete) for computational inelasticity procedures. For simpletons. 

Originally, these notes were written in PDF (using LaTeX) during my PhD days at Caltech. I converted the PDF to a format I thought would be more appealing to people to access and read. You can preview and buy a Kindle version of the book here:

[https://www.amazon.com/dp/B07NL8YF36](https://www.amazon.com/dp/B07NL8YF36) 

Note: if you do not have a Kindle device, Kindle apps for PC and Mac can be used to read the document.

Typos are listed in this [document](./typos/typos.pdf).

For general inquiries, please email: kengwit@gmail.com


![](./cover.png)

